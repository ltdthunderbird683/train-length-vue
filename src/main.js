import Vue from 'vue'
import App from './App.vue'
import firebase from 'firebase'
import VueAdsense from 'vue-adsense'


/* ここからfontawesome */
import { library } from '@fortawesome/fontawesome-svg-core'
import { faSearch } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
library.add(faSearch)
Vue.component('font-awesome-icon', FontAwesomeIcon)
/* ここまでfontawesome */

/*adsense*/
Vue.component('adsense',VueAdsense)
/*adsenseおわり*/

/* firebase設定 */
Vue.config.productionTip = false

var firebaseConfig = {
  apiKey: "AIzaSyAhQKCv2DKg-_BuRspz0IoAkw3q78EDcBY",
  authDomain: "traininfo-8a4c2.firebaseapp.com",
  databaseURL: "https://traininfo-8a4c2.firebaseio.com",
  projectId: "traininfo-8a4c2",
  storageBucket: "traininfo-8a4c2.appspot.com",
  messagingSenderId: "1005475049710",
  appId: "1:1005475049710:web:d012ec235a14022ca243ff",
  measurementId: "G-WLVNPKKQM4"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

new Vue({
  render: h => h(App),
}).$mount('#app')
